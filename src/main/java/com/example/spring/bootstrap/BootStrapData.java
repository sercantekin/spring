package com.example.spring.bootstrap;

import com.example.spring.controllers.BookController;
import com.example.spring.domain.Author;
import com.example.spring.domain.Book;
import com.example.spring.domain.Publisher;
import com.example.spring.repositories.AuthorRepository;
import com.example.spring.repositories.BookRepository;
import com.example.spring.repositories.PublisherRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

// here we have connection between entities and crud repositories
@Component // means that this spring managed component
public class BootStrapData implements CommandLineRunner {
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;
    private final PublisherRepository publisherRepository;

    public BootStrapData(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        // creating publisher and adding to h2 database
        Publisher publisher = new Publisher();
        publisher.setName("SFG Publishing");
        publisher.setCity("ST Petersburg");
        publisher.setState("FL");
        publisherRepository.save(publisher); // since we use set attributes here, we need to save publisher

        // creating author and book objects
        Author eric = new Author("Eric", "Evans");
        Book ddd = new Book("Domain Driven Design", "123123");

        // setting relationship
        eric.getBooks().add(ddd);
        ddd.getAuthors().add(eric);
        ddd.setPublisher(publisher);
        publisher.getBooks().add(ddd);

        // adding object to h2 database
        authorRepository.save(eric);
        bookRepository.save(ddd);
        bookRepository.save(ddd); // save did not work in a way to add record second time here
        publisherRepository.save(publisher);

        System.out.println(bookRepository.count());
    }
}
