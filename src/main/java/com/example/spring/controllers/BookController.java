package com.example.spring.controllers;

import com.example.spring.repositories.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

// controllers handle http request. it means that when a request is received, this controller returns the content of requested page
@Controller // this will tell IDE that this is spring controller
public class BookController {
    private final BookRepository bookRepository; // what we will return

    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @RequestMapping("/books") // requested path
    public String getBooks(Model model){
        model.addAttribute("books", bookRepository.findAll()); // books name will keep all books in repo and it will be mapped in html
        return "books/list"; // list is name of our html in resources, it is under templates/books directory
    }
}
