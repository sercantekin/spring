package com.example.spring.repositories;

import com.example.spring.domain.Book;
import org.springframework.data.repository.CrudRepository;

// allows us to use basic queries with our own parameters. here we have object and id
public interface BookRepository extends CrudRepository<Book, Long> {}
